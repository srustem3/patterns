import java.util.Iterator;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class Stomatology extends Organisation {
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Stomatology cloned = (Stomatology) super.clone();
        cloned.setCity("Москва");
        cloned.setName("Базовая стоматология");
        return cloned;
    }

    @Override
    public String showAll() {
        StringBuilder result = new StringBuilder("Список пациентов: \n");
        Iterator<String> patientsIterator = people.iterator();
        while (patientsIterator.hasNext()) {
            result.append(patientsIterator.next() + "\n");
        }
        return result.toString();
    }
}
