import java.util.Iterator;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class Fitness extends Organisation {
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Fitness cloned = (Fitness) super.clone();
        cloned.setCity("Москва");
        cloned.setName("Базовый фитнес");
        return cloned;
    }

    @Override
    public String showAll() {
        StringBuilder result = new StringBuilder("Список спортсменов: \n");
        Iterator<String> patientsIterator = people.iterator();
        while (patientsIterator.hasNext()) {
            result.append(patientsIterator.next() + "\n");
        }
        return result.toString();
    }
}
