/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class OpenedStrategy implements StrategyInterface {
    @Override
    public String getStatus() {
        return "Открыто";
    }
}
