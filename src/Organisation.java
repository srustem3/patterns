import java.util.ArrayList;

/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public abstract class Organisation implements Cloneable {
    private String name;
    private String city;
    protected ArrayList<String> people;
    private StrategyInterface strategy;

    public static Organisation fabricMethod(String type) throws Exception {
        if (type.equals("Стоматология")) {
            return (Stomatology) new Stomatology().clone();
        } else if (type.equals("Фитнес")) {
            return (Fitness) new Fitness().clone();
        } else {
            throw new Exception("Неизвестный тип предприятия");
        }
    }

    public String getStatus() {
        return strategy.getStatus();
    }

    public void setStrategy(StrategyInterface strategy) {
        this.strategy = strategy;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public abstract String showAll();
}
