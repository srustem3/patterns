/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public class ClosedStrategy implements StrategyInterface {
    @Override
    public String getStatus() {
        return "Закрыто";
    }
}
