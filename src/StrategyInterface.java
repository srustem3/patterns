/**
 * @author Rustem Saitgareev
 *         11-602
 *         000
 */
public interface StrategyInterface {
    String getStatus();
}
